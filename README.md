# install host update
vagrant plugin install vagrant-hostsupdater

# keep vb-guest up to date with virtualbox
vagrant plugin install vagrant-vbguest

# start vms
vagrant up

# vagrant ansible playbook
vagrant provision
